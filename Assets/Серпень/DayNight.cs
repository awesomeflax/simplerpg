﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour
{
    public Transform SkyLight;

    float rotationSpeed = 0.7f;

	void Start ()
    {

	}
	
	void Update ()
    {
        transform.Rotate(Vector3.up * (rotationSpeed * Time.deltaTime ));
        transform.Rotate(Vector3.right * (rotationSpeed * Time.deltaTime));
        //transform.Rotate(Vector3.forward * (Time.deltaTime));
    }
}
