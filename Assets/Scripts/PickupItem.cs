﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : Interectable
{
    // перегрузка взаимодействия для предмета, который можно поднимать (промежуточный)
    public override void Interact()
    {
        Debug.Log("interaction from the pickup item class");
    }
}
