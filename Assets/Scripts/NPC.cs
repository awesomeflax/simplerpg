﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Interectable
{
    public string[] dialogue;
    public string _name;

    // перегрузка связи с персонажем для конкретного NPC с использрванием диалога
    public override void Interact()
    {
        DialogueSystem.Instance.AddNewDialogue(dialogue, _name);
        Debug.Log("Interact with NPC class called " + _name.ToString());
    }
}
