﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats
{
    public CharacterStats(int power, int toughness,  int attackSpeed, int sorcery)
    {
        stats = new List<BaseStat>()
        {
            new BaseStat(BaseStat.BaseStatType.Power, power, "Power"),
            new BaseStat(BaseStat.BaseStatType.Toughness, toughness, "Toughness"),
            new BaseStat(BaseStat.BaseStatType.AttackSpeed, attackSpeed, "Attack speed"),
            new BaseStat(BaseStat.BaseStatType.Sorcery, sorcery, "Sorcery"),
        };
    }

    public List<BaseStat> stats = new List<BaseStat>();

    public BaseStat GetStat(BaseStat.BaseStatType stat)
    {
        return this.stats.Find(x => x.StatType == stat);
    }

    public void AddStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            GetStat(statBonus.StatType).AddStatBonus(new BonusStat(statBonus.BaseValue));
        }
    }

    public void RemoveStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            GetStat(statBonus.StatType).RemoveStatBonus(new BonusStat(statBonus.BaseValue));
        }
    }
}
