﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ItemDatabase : MonoBehaviour
{
    public static ItemDatabase Instance;
    private List<Item> Items { get; set; }

    void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;

        BuildDatabase();
    }

    private void BuildDatabase()
    {
        Items = JsonConvert.DeserializeObject<List<Item>>(Resources.Load<TextAsset>("JSON/Items").ToString());
    }

    public Item GetItem(string _itemSlug)
    {
        foreach(Item item in Items)
        {
            if (item.ObjectSlug.Equals(_itemSlug))
                return item;
        }

        Debug.Log("There is no such item called " + _itemSlug);
        return null;
    }
}
