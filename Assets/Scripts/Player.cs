﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public CharacterStats characterStats;

    void Start()
    {
        characterStats = new CharacterStats(10, 10, 10, 0);

        Debug.Log("So you born with " + characterStats.GetStat(BaseStat.BaseStatType.Power).GetCalculatedStatValue() + " power");
        Debug.Log("and " + characterStats.GetStat(BaseStat.BaseStatType.AttackSpeed).GetCalculatedStatValue() + " attack speed");
        Debug.Log("and " + characterStats.GetStat(BaseStat.BaseStatType.Sorcery).GetCalculatedStatValue() + " magic power");
        Debug.Log("and " + characterStats.GetStat(BaseStat.BaseStatType.Toughness).GetCalculatedStatValue() + " toughness");
    }
}
