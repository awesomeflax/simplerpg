﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    public float playerCameraDistance { get; set; }
    public Transform cameraTarget;

    Camera playerCamera;
    float zoomSpeed = 25f;

    void Start()
    {
        playerCameraDistance = 10f;
        playerCamera = GetComponent<Camera>();
    }

    void Update()
    {
        cameraZoom();
    }
    
    void cameraZoom()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            playerCamera.fieldOfView -= scroll * zoomSpeed;
            playerCamera.fieldOfView = Mathf.Clamp(playerCamera.fieldOfView, 15, 70);
        }

        transform.position = new Vector3(cameraTarget.position.x, cameraTarget.position.y + playerCameraDistance, cameraTarget.position.z - playerCameraDistance);
    }
}
