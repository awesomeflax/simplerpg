﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// класс объекта бонусного стата. 
public class BonusStat 
{
    public int BonusValue { get; set; }

    public BonusStat(int bonusValue)
    {
        this.BonusValue = bonusValue;
    }
}
