﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionItem : Interectable
{
    public string[] dialogue;

    // взаимодействие с предметом, с которым можно его произвести (промежуточный)
    public override void Interact()
    {
        Debug.Log("interction with an action item");
    }
}
