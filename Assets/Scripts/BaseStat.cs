﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class BaseStat
{
    public enum BaseStatType { Power, Toughness, AttackSpeed, Sorcery }

    public List<BonusStat> BaseAdditives { get; set; }
    [JsonConverter(typeof(StringEnumConverter))]
    public BaseStatType StatType { get; set; }
    public int BaseValue { get; set; }
    public string StatName { get; set; }
    public string StatDescription { get; set; }
    public int FinalValue { get; set; }

    // создание нового стата со всеми его свойствами (имя, значение, описание)
    public BaseStat(int baseValue, string statName, string statDescription)
    {
        this.BaseAdditives = new List<BonusStat>();
        this.StatName = statName;
        this.BaseValue = baseValue;
        this.StatDescription = statDescription;
    }

    // для Json.NET
    [Newtonsoft.Json.JsonConstructor]
    public BaseStat(BaseStatType statType, int baseValue, string statName)
    {
        this.BaseAdditives = new List<BonusStat>();
        this.StatType = statType;
        this.StatName = statName;
        this.BaseValue = baseValue;
    }

    public void AddStatBonus(BonusStat statBonus)
    {

        this.BaseAdditives.Add(statBonus);
    }

    public void RemoveStatBonus(BonusStat statBonus)
    {
        this.BaseAdditives.Remove(BaseAdditives.Find(x => x.BonusValue == statBonus.BonusValue));
    }

    public int GetCalculatedStatValue()
    {
        this.FinalValue = 0;
        this.BaseAdditives.ForEach(x => this.FinalValue += x.BonusValue);
        this.FinalValue += BaseValue;

        return FinalValue;
    }
}
