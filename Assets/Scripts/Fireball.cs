﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public Vector3 Direction { get; set; }
    public CharacterStats CharacterStats { get; set; }
    public float Range { get; set; }
    public int Damage { get; set; }

    Vector3 spawnPosition;

    public List<BaseStat> Stats { get; set; }

    void Start()
    {
        Range = 20f;
        Damage = 5;
        spawnPosition = transform.position;
        GetComponent<Rigidbody>().AddForce(Direction * 100f);
    }

    void Update()
    {
        if (Vector3.Distance(spawnPosition, transform.position) >= Range)
        {
            Debug.Log("Normal distance death");
            Extinquish();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "Enemy")
        {
            col.transform.GetComponent<IEnemy>().TakeDamage(Damage);
            Debug.Log("Enemy got " + Damage.ToString() + " pure damage from fireball");
        }

        Debug.Log("Collision death with " + col.transform.name.ToString());

        Extinquish();
    }

    void Extinquish()
    {
        Destroy(gameObject);
    }
}
