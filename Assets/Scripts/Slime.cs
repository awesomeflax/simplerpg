﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NPC))]
public class Slime : Interectable, IEnemy
{
    public float currentHealh, power, toughness;
    public float maxHealth;

    private CharacterStats characterStats;

    void Start()
    {
        characterStats = new CharacterStats(6, 60, 2, 0);
        currentHealh = maxHealth;
    }

    public void PerfomAttack()
    {
        throw new System.NotImplementedException();
    }

    public void TakeDamage(int amount)
    {
        currentHealh -= amount;

        if (currentHealh <= 0)
            Die();
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
